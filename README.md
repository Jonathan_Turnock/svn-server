# svn-server

Basic Subversion server installed on Ubuntu 16.04 including a pre-configured apache2 installation.

Designed for Home or Development Environments where security is not a concern.

It has the following weaknesses      
1. Open SSH Server Installed   
2. Logins are enabled for root user   
3. Root User Password is passed in at container start   

## Installation

Download and run the container in docker, run with the provided docker-compose file if necessary

## Usage

apache is available on port 80 and the svn server is accessible at /svn

THIS IS AN INSECURE SVN SERVER AND SHOULD NOT BE USED IN A PRODUCTION ENVIRONMENT WITHOUT HARDENING

Python Script on the root volume /onCreate.py will be run once on creation of the image
Python Script on the root volume /onStart.py will be run each time the image is ran

A Volume is declared at /var/svn/repos which is where repositories are created when pushed to the svn server

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)